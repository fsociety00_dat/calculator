package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private var firstVariable:Double = 0.0
    private var secondVariable:Double = 0.0
    private var operation = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init() {
        n0Button.setOnClickListener(this)
        n1Button.setOnClickListener(this)
        n2Button.setOnClickListener(this)
        n3Button.setOnClickListener(this)
        n4Button.setOnClickListener(this)
        n5Button.setOnClickListener(this)
        n6Button.setOnClickListener(this)
        n7Button.setOnClickListener(this)
        n8Button.setOnClickListener(this)
        n9Button.setOnClickListener(this)

        backspaceButton.setOnLongClickListener{
            resultTextView.text = ""
            return@setOnLongClickListener true
        }



        pointButton.setOnClickListener {
            if (resultTextView.text.isNotEmpty()) {
                resultTextView.text = resultTextView.text.toString() + "."
                pointButton.isClickable = false
            }
        }
    }

    fun equal(view: View){
        val value = resultTextView.text.toString()
        if (value.isNotEmpty() && operation.isNotEmpty()) {
            secondVariable = value.toDouble()
            var result:Double = 0.0
            if (operation == "/") {
                if (secondVariable == 0.0) {
                    resultTextView.setText("can't divide by 0!");
                } else {
                    result = firstVariable/secondVariable;
                    resultTextView.text = result.toString()
                }
            }
            if (operation == "+"){
                result = firstVariable + secondVariable
                resultTextView.text = result.toString()
            }
            if (operation == "-"){
                result = firstVariable - secondVariable
                resultTextView.text = result.toString()
            }
            if (operation == "X"){
                result = firstVariable * secondVariable
                resultTextView.text = result.toString()
            }


            operation = ""
            firstVariable = 0.0
            secondVariable = 0.0
        }
    }

    fun add(view: View){
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            firstVariable = value.toDouble()
            operation = "+"
            resultTextView.text = ""
        }

    }

    fun subtraction(view: View){
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            firstVariable = value.toDouble()
            operation = "-"
            resultTextView.text = ""
        }

    }

    fun multiply(view: View){
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            firstVariable = value.toDouble()
            operation = "X"
            resultTextView.text = ""
        }

    }

    fun divide(view: View){
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            firstVariable = value.toDouble()
            operation = "/"
            resultTextView.text = ""
        }
    }

    fun backspace(view: View){
        val value = resultTextView.text.toString()
        if (value.isNotEmpty())
            resultTextView.text = value.substring(0, value.length - 1)
            if (resultTextView.text.contains(".") == false){
                pointButton.isClickable = true
            }
    }

    override fun onClick(v: View?) {
        val button = v as Button
        resultTextView.text = resultTextView.text.toString() + button.text.toString()
    }
}